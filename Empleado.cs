﻿namespace JoselynTorres3BEmpresa
{
    public class Empleado
    {
        private string Nombres;
        private string Apellidos;
        private int Edad;
        private string Departamento;

        public Empleado(string Nombres, string Apellidos, int Edad, string Departamento)
        {
            this.Nombres = Nombres;
            this.Apellidos = Apellidos;
            this.Edad = Edad;
            this.Departamento = Departamento;
        }

        public string getnombres()
        {
            return Nombres;
        }
        public void setnombres(string Nombres)
        {
            this.Nombres = Nombres;
        }
        public string getapellidos()
        {
            return Apellidos;
        }
        public void setapellidos(string Apellidos)
        {
            this.Apellidos = Apellidos;
        }
        public int getedad()
        {
            return Edad;
        }
        public void setedad(int Edad)
        {
            this.Edad = Edad;
        }
        public string getdepartamento()
        {
            return Departamento;
        }
        public void setdepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }
        public void MostrarDatos()
        {
            Console.WriteLine("Nombres del empleado:" + Nombres);
            Console.WriteLine("Apellidos del empleado:" + Apellidos);
            Console.WriteLine("Edad del empleado:" + Edad);
            Console.WriteLine("Departamento del empleado:" + Departamento);
        }
    }
}
