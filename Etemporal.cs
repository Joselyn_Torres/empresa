﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoselynTorres3BEmpresa
{
    public class Etemporal:Empleado
    {
        private string Fechaingreso;
        private string Fechasalida;
        

        public Etemporal(string Nombres, string Apellidos, int Edad, string Departamento, string Fechaingreso, string Fechasalida ): base(Nombres, Apellidos, Edad, Departamento)
        {
            this.Fechaingreso = Fechaingreso;
            this.Fechasalida = Fechasalida;
        }
        
        public string getfechaingreso()
        {
            return Fechaingreso;
        }
        public void setfechaingreso( string Fechaingreso)
        {
            this.Fechaingreso = Fechaingreso;
        }
        public string getfechasalida()
        {
            return Fechasalida;
        }
        public void setfechasalida(string Fechasalida)
        {
            this.Fechasalida = Fechasalida;
        }
        public void MostrarSueldo()
        {
            int Sueldofijo=600;
            Console.WriteLine("El sueldo fijo del empleado es: " + Sueldofijo);
        }
    }
}
