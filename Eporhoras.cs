﻿namespace JoselynTorres3BEmpresa
{
    public class Eporhoras : Empleado
    {
        private int Horastrabajadas;
        private int Horaspormes;
        public int Valorporhoras;
        public int Sueldoporhoras;

        public Eporhoras(string Nombres, string Apellidos, int Edad, string Departamento, int Horastrabajadas, int Horaspormes, int Valorporhoras, int Sueldoporhoras, int SueldoTotal) : base(Nombres, Apellidos, Edad, Departamento)
        {

        }

        public int gethorastrabajadas()
        {
            return Horastrabajadas;
        }
        public void sethorastrabajadas(int Horastrabajadas)
        {
            this.Horastrabajadas = Horastrabajadas;
        }
        public int gethoraspormes()
        {
            return Horaspormes;
        }
        public void sethoraspormes(int Horaspormes)
        {
            this.Horaspormes = Horaspormes;
        }
        public void MostrarSueldo()
        {
            int SueldoTotal = 0;
            Console.WriteLine("Ingrese las horas trabajadas: ");
            Horastrabajadas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el valor de horas trabajadas: ");
            Valorporhoras = int.Parse(Console.ReadLine());
            Sueldoporhoras = Horastrabajadas * Valorporhoras;
            Console.WriteLine("El sueldo por horas es de: " + Sueldoporhoras);
            Console.WriteLine("Ingrese las horas trabajadas por mes: ");
            Horaspormes = int.Parse(Console.ReadLine());

            SueldoTotal = Sueldoporhoras * Horaspormes;
            Console.WriteLine("El sueldo del empleado por horas es: " + SueldoTotal);
        }
    }
}
