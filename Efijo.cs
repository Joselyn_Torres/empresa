﻿namespace JoselynTorres3BEmpresa
{
    public class Efijo : Empleado
    {
        private int Añoentrada;
        public int Añoactual;
        public int Sueldobase;
        public int Complementoanual;
        public int Añostrabajados;
        public int Sueldo;

        public Efijo(string Nombres, string Apellidos, int Edad, string Departamento, int Añoentrada, int Añoactual, int Sueldobase, int Complementoanual, int Añostrabajados, int Sueldo) : base(Nombres, Apellidos, Edad, Departamento)
        {

        }

        public int getañoentrada()
        {
            return Añoentrada;
        }
        public void setañoentrada(int Añoentrada)
        {
            this.Añoentrada = Añoentrada;
        }
        public void MostrarSueldo()
        {
            int Sueldo;
            Console.WriteLine("Ingrese el año de entrada: ");
            int Añoentrada = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el año actual: ");
            int Añoactual = int.Parse(Console.ReadLine());
            Console.WriteLine("Los años trabajados son: ");
            Añostrabajados = Añoactual - Añoentrada;
            Console.WriteLine("Ingrese el complemento anual: ");
            int Complementoanual = int.Parse(Console.ReadLine());

            Sueldo = Sueldobase + (Complementoanual * Añostrabajados);
            Console.WriteLine("El sueldo del empleado fijo es: " + Sueldo);
        }
    }
}
