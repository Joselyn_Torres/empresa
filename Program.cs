﻿namespace JoselynTorres3BEmpresa
{
    public class Program
    {
        static void Main(string[] args)
        {

            Empleado empleadoo = new Empleado("Joselyn", "Torres", 19, "Departamento");
            empleadoo.MostrarDatos();

            Console.WriteLine("Empleado temporal:");
            Etemporal etemporal = new Etemporal("Joselyn", "Torres", 19, "Departamento", "14 de marzo", "17 de marzo");
            etemporal.MostrarSueldo();

            Console.WriteLine("Empleado Fijo:");
            Efijo efijo = new Efijo("Joselyn", "Torres", 19, "Departamento", 2015, 2021, 300, 1000, 6, 390);
            efijo.MostrarSueldo();

            Console.WriteLine("Empleado por horas:");
            Eporhoras eporhoras = new Eporhoras("Joselyn", "Torres", 19, "Departamento", "14 de marzo", "17 de marzo", 12, 12, 1000, 100);
            eporhoras.MostrarSueldo();
        }
    }
}
